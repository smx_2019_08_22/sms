package business.sessionBeans;

import entities.Ucenici;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class StudentSessionBean implements StudentSessionBeanLocal {
    
    @PersistenceContext(unitName="SMSPU")
    private EntityManager em;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public List<Ucenici> getAllStudents() {
        
        try {
        Query query = em.createNamedQuery("Ucenici.findAll");
        List <Ucenici> ucenici = (List<Ucenici>) query.getResultList();
        return ucenici;
        }
        catch (NoResultException nre) {
            return null;
        } 
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    } 
    @Override
        public List<Ucenici> getStudentsFromDepartment(int odeljenje) {
        
        try {
        Query query = em.createNamedQuery("Ucenici.findUceniciByOdeljenje");
        query.setParameter("odeljenja", odeljenje);
        List <Ucenici> ucenici = (List<Ucenici>) query.getResultList();
        return ucenici;
        }
        catch (NoResultException nre) {
            return null;
        } 
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
