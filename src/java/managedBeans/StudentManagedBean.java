package managedBeans;

import business.sessionBeans.StudentSessionBeanLocal;
import entities.Ucenici;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "studentMB", eager = true)
@SessionScoped
public class StudentManagedBean implements Serializable{
    
    @EJB
    private StudentSessionBeanLocal studentBean;
    
    public List<Ucenici> getAllStudents() {
        
        return studentBean.getAllStudents();
    }
        public List<Ucenici> getStudentsFromDepartment(int odeljenje) {
        
        return studentBean.getStudentsFromDepartment(odeljenje);
    }
}
