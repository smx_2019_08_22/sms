/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import business.sessionBeans.OceneSessionBeanLocal;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Grupa1
 */
@Named(value = "oceneManagedBean")
@SessionScoped
public class OceneManagedBean {

    @EJB
    private OceneSessionBeanLocal oceneBean; 
    
    public OceneManagedBean() {
    }
    
}
