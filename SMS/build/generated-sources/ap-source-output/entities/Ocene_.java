package entities;

import entities.Predmeti;
import entities.Ucenici;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-27T16:37:53")
@StaticMetamodel(Ocene.class)
public class Ocene_ { 

    public static volatile SingularAttribute<Ocene, Short> zakljucna;
    public static volatile SingularAttribute<Ocene, Ucenici> uceniciId;
    public static volatile SingularAttribute<Ocene, Integer> id;
    public static volatile SingularAttribute<Ocene, Short> ocena;
    public static volatile SingularAttribute<Ocene, Predmeti> predmetiId;

}