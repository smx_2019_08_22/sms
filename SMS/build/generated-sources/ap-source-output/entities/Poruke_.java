package entities;

import entities.Roditelji;
import entities.Ucitelji;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-27T16:37:53")
@StaticMetamodel(Poruke.class)
public class Poruke_ { 

    public static volatile SingularAttribute<Poruke, Roditelji> roditeljiId;
    public static volatile SingularAttribute<Poruke, String> tekst;
    public static volatile SingularAttribute<Poruke, Ucitelji> uciteljiId;
    public static volatile SingularAttribute<Poruke, Integer> id;

}